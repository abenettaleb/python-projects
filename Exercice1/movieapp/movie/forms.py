from django import forms
from .models import Movie
from django.core.exceptions import ValidationError


class MovieCreate(forms.ModelForm):
    class Meta:
        model = Movie
        # fields = '__all__
        fields = ('titre', 'poster', 'realisateur', 'annee', 'details')
        widgets = {
            'titre': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'poster': forms.FileInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'realisateur': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'annee': forms.NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'details': forms.Textarea(
                attrs={
                    'class': 'form-control'
                }
            ),
        }
