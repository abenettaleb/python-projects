from django.db import models

class Movie(models.Model):
    titre = models.CharField(max_length = 50)
    poster = models.ImageField()
    realisateur = models.CharField(max_length = 30, default='inconu')
    annee = models.IntegerField()
    details = models.TextField(default = 'Détails...')
    def __str__(self):
        return self. titre
