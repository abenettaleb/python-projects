from django.shortcuts import render, redirect
from .models import Movie
from .forms import MovieCreate
from django.http import HttpResponse


def index(request):
    shelf = Movie.objects.all()
    return render(request, 'movie/index.html', {'shelf': shelf})


def add(request):
    upload = MovieCreate()
    if request.method == 'POST':
        upload = MovieCreate(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('index')
        else:
            return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'movie/addmovie.html', {'add': upload})


def update(request, movie_id):
    movie_id = int(movie_id)
    try:
        movie_sel = Movie.objects.get(id=movie_id)
    except Movie.DoesNotExist:
        return redirect('index')
    movie_form = MovieCreate(request.POST or None, instance=movie_sel)
    if movie_form.is_valid():
        movie_form.save()
        return redirect('index')
    return render(request, 'movie/addmovie.html', {'add': movie_form})


def delete_movie(request, movie_id):
    movie_id = int(movie_id)
    try:
        movie_sel = Movie.objects.get(id=movie_id)
    except Movie.DoesNotExist:
        return redirect('index')
    movie_sel.delete()
    return redirect('index')
