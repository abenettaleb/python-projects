from django.db import models


class Etudiant(models.Model):
    cne = models.CharField(max_length=10, blank=False, default='')
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    email = models.CharField(max_length=150)
    tel = models.CharField(max_length=14)
    datenaissance = models.DateField()
    ville = models.CharField(max_length=100)
    filiere = models.CharField(max_length=50)
