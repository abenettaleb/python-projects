from rest_framework import serializers
from etudiants.models import Etudiant


class EtudiantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Etudiant
        fields = ('id',
                  'cne',
                  'nom',
                  'prenom',
                  'email',
                  'tel',
                  'datenaissance',
                  'ville',
                  'filiere')
