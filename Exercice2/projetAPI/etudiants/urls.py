from django.conf.urls import url
from etudiants import views

urlpatterns = [
    url(r'^api/etudiants$', views.index),
    url(r'^api/etudiants/(?P<pk>[0-9]+)$', views.etud),
]
