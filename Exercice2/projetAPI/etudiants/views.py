from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from etudiants.models import Etudiant
from etudiants.serializers import EtudiantSerializer
from rest_framework.decorators import api_view


@api_view(['GET', 'POST', 'DELETE'])
def index(request):
    if request.method == 'GET':
        etudiants = Etudiant.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            etudiants = etudiants.filter(title__icontains=title)

        etudiants_serializer = EtudiantSerializer(etudiants, many=True)
        return JsonResponse(etudiants_serializer.data, safe=False)
        # 'safe=False' for objects serialization
    elif request.method == 'POST':
        etudiant_data = JSONParser().parse(request)
        etudiants_serializer = EtudiantSerializer(data=etudiant_data)
        if etudiants_serializer.is_valid():
            etudiants_serializer.save()
            return JsonResponse(etudiants_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(etudiants_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Etudiant.objects.all().delete()
        return JsonResponse({'message': '{} Etudiant were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def etud(request, pk):
    try:
        etudiant = Etudiant.objects.get(pk=pk)
    except Etudiant.DoesNotExist:
        return JsonResponse({'message': 'Etudiant does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        etudiants_serializer = EtudiantSerializer(etudiant)
        return JsonResponse(etudiants_serializer.data)

    elif request.method == 'PUT':
        etudiant_data = JSONParser().parse(request)
        etudiants_serializer = EtudiantSerializer(etudiant, data=etudiant_data)
        if etudiants_serializer.is_valid():
            etudiants_serializer.save()
            return JsonResponse(etudiants_serializer.data)
        return JsonResponse(etudiants_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        etudiant.delete()
        return JsonResponse({'message': 'Etudiant was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
