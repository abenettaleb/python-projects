from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from .models import Etudiant
from .serializers import EtudiantSerializer
from rest_framework.decorators import api_view
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response


@csrf_exempt
@api_view(['GET', 'POST', 'DELETE'])
# @permission_classes((AllowAny,))
def etudiants(request):
    if request.method == 'GET':
        etudiants = Etudiant.objects.all()
        etudiants_serializer = EtudiantSerializer(etudiants, many=True)
        return JsonResponse(etudiants_serializer.data, safe=False)

    elif request.method == 'POST':
        etudiant_serializer = EtudiantSerializer(data=request.data)
        if etudiant_serializer.is_valid():
            etudiant_serializer.save()
            return JsonResponse(etudiant_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse(etudiant_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Etudiant.objects.all().delete()
        return JsonResponse({'message': '{} Etudiant were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def etud(request, pk):
    try:
        etudiant = Etudiant.objects.get(pk=pk)
    except Etudiant.DoesNotExist:
        return JsonResponse({'message': 'Etudiant does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        etudiants_serializer = EtudiantSerializer(etudiant)
        return JsonResponse(etudiants_serializer.data)

    elif request.method == 'PUT':
        etudiant_data = JSONParser().parse(request)
        etudiants_serializer = EtudiantSerializer(etudiant, data=etudiant_data)
        if etudiants_serializer.is_valid():
            etudiants_serializer.save()
            return JsonResponse(etudiants_serializer.data)
        return JsonResponse(etudiants_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        etudiant.delete()
        return JsonResponse({'message': 'Etudiant was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
